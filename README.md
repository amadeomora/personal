# Personal

### IES Virgen de la Paloma ###

**Módulos:**

- Acceso a datos
- Programación multimedia y dispositivos móviles

**Enlaces:**

- [Página del docente](https://www.educa2.madrid.org/web/amadeo.mora)
- [Página del centro](https://palomafp.org)

---

Kotlin:

    fun main() {
        println("Hola mundo")
    }

Java:

    class HolaMundo {
        public static void main(String[] args) {
            System.out.println("Hola mundo");
        }
    }

PHP:

    <?php
        echo "Hola mundo\n";
    ?>

C:

    #include <stdio.h>

    int main(int argc, char * argv[]) {
        printf("Hola mundo\n");
    }

---

